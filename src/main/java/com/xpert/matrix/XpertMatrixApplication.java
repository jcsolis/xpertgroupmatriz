package com.xpert.matrix;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.springframework.boot.CommandLineRunner;
import com.xpert.matrix.controller.ProcesarMatrizController;
import com.xpert.matrix.valid.ControlErrores;


public class XpertMatrixApplication implements CommandLineRunner {
	
	private static ProcesarMatrizController procesarMatrizController = new ProcesarMatrizController(); 
	
	
	public static void main(String[] args) throws IOException, ControlErrores   {
		//SpringApplication.run(XpertMatrixApplication.class, args);
		String entrada = null;

		Long T;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
		System.out.println("Ingrese componentes para el calculo: "); 
		entrada = br.readLine();
		T = Long.parseLong(entrada);
		
	
		procesarMatrizController.ingresarDatos(T);
	}


	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	

	/*public void run(String... args) throws IOException, ControlErrores {
		String entrada = null;

		Long T;
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
		System.out.println("Ingrese el numero de casos: "); 
		entrada = br.readLine();
		T = Long.parseLong(entrada);
		
	
		procesarMatrizController.ingresarDatos(T);
		
	}*/

}
