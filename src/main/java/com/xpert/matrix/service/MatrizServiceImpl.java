package com.xpert.matrix.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;

import com.xpert.matrix.valid.ControlErrores;
import com.xpert.matrix.valid.ValidarComponentes;

public class MatrizServiceImpl implements IMatrizService {
	
	private Long matriz[][][] = new Long[101][101][101];
	private ArrayList<String> resultados = new ArrayList<String>();
	private ValidarComponentes validador = new ValidarComponentes(); 

	@Override
	public void llenarMatriz() {
		for(int ejeX = 0; ejeX < matriz.length; ejeX++) {
			for(int ejeY=0; ejeY < matriz.length; ejeY++) {
				for(int ejeZ=0; ejeZ < matriz.length; ejeZ++) {
					matriz[ejeX][ejeY][ejeZ] = 0L;
				}
			}
		}
		
	}

	@Override
	public Long calcularResultadosEnLaMatriz(Long ejeX, Long ejeY, Long ejeZ) {
		
		Long ejeXPrima,ejeYPrima,resultado = 0L; 

		while(ejeZ > 0) {
			ejeXPrima = ejeX; 
			while(ejeXPrima > 0) {
				ejeYPrima = ejeY;
				while(ejeYPrima > 0) {
					resultado +=  matriz[ejeXPrima.intValue()][ejeYPrima.intValue()][ejeZ.intValue()];
					ejeYPrima -= (ejeYPrima & -ejeYPrima);
				}
				ejeXPrima -= (ejeXPrima & -ejeXPrima);
			}
			ejeZ -= (ejeZ & -ejeZ);  
		}
		
		return resultado;
		
	}

	@Override
	public void actualizarValoresEnLaMatriz(Long tamanoMatriz, Long ejeX, Long ejeY, Long ejeZ, Long valor) {

		Long ejeYPrima, ejeXPrima; 
		
		while(ejeZ <= tamanoMatriz) {
			ejeXPrima = ejeX;
			while(ejeXPrima <= tamanoMatriz) {
				ejeYPrima = ejeY;
				while(ejeYPrima <= tamanoMatriz) {
				matriz[ejeXPrima.intValue()][ejeYPrima.intValue()][ejeZ.intValue()] += valor;
				ejeYPrima += (ejeYPrima & -ejeYPrima);
				}
				ejeXPrima += (ejeXPrima & -ejeXPrima);
			}
			ejeZ += (ejeZ & -ejeZ);	
		}		
		
	}


	@Override
	public void ingresarDatos(Long T) throws IOException, ControlErrores {
		
		ArrayList<String> cargarDatos = new ArrayList<String>();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String entrada = null;		
		
		Long n;		
		Long m;
		
		if (validador.validarCasos(T)) {
			throw new ControlErrores(111);
		}
		
		while(T > 0) {
			
			entrada = br.readLine(); 

			String[] nm = entrada.split(" ");
			
			n = Long.parseLong(nm[0]);
			
			if (validador.validarDimemsionMatriz(n)) {
				throw new ControlErrores(112);
			}
			
			m = Long.parseLong(nm[1]);
			
			if (validador.validarOperaciones(m)) {
				throw new ControlErrores(113);
			}			
			cargarDatos.add(entrada);
			
			while (m > 0) {
				entrada = br.readLine();
				cargarDatos.add(entrada);
				m--;
			}
						
			procerDatosMatriz(cargarDatos, n);
			cargarDatos.clear();
			
			T--;
		}
		
		imprimirResultados(resultados);
		
	}

	@Override
	public void procerDatosMatriz(ArrayList<String> procesarMatriz, Long n) throws ControlErrores {
		
		Iterator<String> recorrerDatos = procesarMatriz.iterator();
		
		
		Long x0, y0, z0, x, y, z; 
		Long value1, value2, val, resultado; 

		value1 = 0L; 
		value2 = 0L;  
		val = 0L;
		resultado = 0L; 
		
		llenarMatriz();
						
		
		while(recorrerDatos.hasNext()) {
			String elemento = recorrerDatos.next();			
			
			String[] cadena = elemento.split(" "); 


			if(cadena[0].toUpperCase().equals("QUERY")) {
				
				
				x0 = Long.parseLong(cadena[1]); 
				y0 = Long.parseLong(cadena[2]);
				z0 = Long.parseLong(cadena[3]);

				x = Long.parseLong(cadena[4]); 
				y = Long.parseLong(cadena[5]);
				z = Long.parseLong(cadena[6]);
				
				if (validador.validarEjes(x0, x, n)) {
					throw new ControlErrores(114);
				}
				if (validador.validarEjes(y0, y, n)) {
					throw new ControlErrores(114);
				}				
				if (validador.validarEjes(z0, z, n)) {
					throw new ControlErrores(114);
				}				
				
				value1 = calcularResultadosEnLaMatriz(x,y,z) - calcularResultadosEnLaMatriz(x0 - 1,y,z)
						 - calcularResultadosEnLaMatriz(x,y0 - 1,z) + calcularResultadosEnLaMatriz(x0 - 1,y0 - 1,z);
				
				value2 = calcularResultadosEnLaMatriz(x,y,z0 - 1) - calcularResultadosEnLaMatriz(x0 - 1,y,z0 - 1)
						- calcularResultadosEnLaMatriz(x,y0 - 1,z0 - 1) + calcularResultadosEnLaMatriz(x0 - 1,y0 - 1,z0 - 1);
				
				
				resultado = value1 - value2;
				
				resultados.add(resultado.toString());
				
				
								
			}

			if(cadena[0].toUpperCase().equals("UPDATE") ) {
				
				x = Long.parseLong(cadena[1]); 
				y = Long.parseLong(cadena[2]);
				z = Long.parseLong(cadena[3]);
				val = Long.parseLong(cadena[4]);
				
				x0 = x; 
				y0 = y;
				z0 = z;
				
				if (validador.validarRango(x, n)) {
					throw new ControlErrores(115);
				}				
				if (validador.validarRango(y, n)) {
					throw new ControlErrores(115);
				}				
				if (validador.validarRango(z, n)) {
					throw new ControlErrores(115);
				}	
				if (validador.validarValor(val)) {
					throw new ControlErrores(116);
				}					

				
				value1 = calcularResultadosEnLaMatriz(x,y,z) - calcularResultadosEnLaMatriz(x0 - 1,y,z)
						- calcularResultadosEnLaMatriz(x,y0 - 1,z) + calcularResultadosEnLaMatriz(x0 - 1,y0 - 1,z);
				
				value2 = calcularResultadosEnLaMatriz(x,y,z0 - 1) - calcularResultadosEnLaMatriz(x0 - 1,y,z0 - 1)
						- calcularResultadosEnLaMatriz(x,y0 - 1,z0 - 1) + calcularResultadosEnLaMatriz(x0 - 1,y0 - 1,z0 - 1);
				
				actualizarValoresEnLaMatriz(n, x, y, z, val - (value1 - value2));
				
			}				
			
			
		}
		

	}

	@Override
	public void imprimirResultados(ArrayList<String> resultados) {
		Iterator<String> recorrerResultados = resultados.iterator();
		
		System.out.println("Resultados: "+resultados.size());
		while(recorrerResultados.hasNext()) {
			System.out.println(recorrerResultados.next());
		}
	}
}
