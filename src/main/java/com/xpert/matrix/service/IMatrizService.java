package com.xpert.matrix.service;

import java.io.IOException;
import java.util.ArrayList;

import com.xpert.matrix.valid.ControlErrores;

public interface IMatrizService {

	public void llenarMatriz();
	
	public Long calcularResultadosEnLaMatriz(Long ejeX, Long ejeY, Long ejeZ);
	
	public void actualizarValoresEnLaMatriz(Long tamanoMatriz, Long ejeX, Long ejeY, Long ejeZ, Long valor);
	
	public void imprimirResultados(ArrayList<String> resultados);
	
	public void ingresarDatos(Long T) throws IOException, ControlErrores; 
	
	public void procerDatosMatriz(ArrayList<String> procesarMatriz, Long n) throws ControlErrores;
	

}
