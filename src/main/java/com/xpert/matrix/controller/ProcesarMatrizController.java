package com.xpert.matrix.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.xpert.matrix.service.MatrizServiceImpl;
import com.xpert.matrix.valid.ControlErrores;

public class ProcesarMatrizController {
	
	@Autowired
	private MatrizServiceImpl matrizService = new MatrizServiceImpl(); 
	
	
	public void ingresarDatos(Long T) throws IOException, ControlErrores {
		matrizService.ingresarDatos(T);
	}
	
	

}
