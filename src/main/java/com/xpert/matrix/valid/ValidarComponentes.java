package com.xpert.matrix.valid;

public class ValidarComponentes {
	
		
	public boolean validarCasos(Long T) {
		return (T > 0 && T <= 50)? false: true; 
	}
	
	public boolean validarDimemsionMatriz(Long N) {
		return (N > 0 && N <= 100)? false: true; 
	}
	
	public boolean validarOperaciones(Long M) {
		return (M > 0 && M <= 100)? false: true; 
	}

	public boolean validarEjes(Long inicial, Long Final, Long N) {
		if ((inicial > 0 && inicial <= N) && (Final > 0 && Final <= N) && (inicial <= Final)) {
			return false; 
		}
		return true; 
	}	
	
	public boolean validarRango(Long eje, Long N) {
		return (eje > 0 && eje <= N)? false: true;
	}
	
	public boolean validarValor(Long W) {
		return (W >= -Math.pow(10, 9) && W <= Math.pow(10, 9)) ? false: true; 
	}

}
